from __future__ import unicode_literals
from django.db import models

# Create your models here.
class User(models.Model):
    email = models.EmailField()
    password = models.CharField(max_length=20)
    full_name = models.CharField(max_length=100)
    avatar = models.ImageField(upload_to="static/img")


class BonusCard(models.Model):  # Бонусная карта
    card_number = models.CharField(max_length=12)
    pin_code = models.CharField(max_length=4)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Movie(models.Model):  # Фильм
    GENRE_CHOICES = (("Thriller", "Триллер"), ("Horror", "Ужасы"), ("Comedy", "Комедия"), ("Drama", "Мелодрама"),
                     ("Cartoon", "Мульфильм"), ("Educational", "Образовательное"), ("Fantasy","Фантастика"),
                     ("Shooter", "Боевик"), ("Musical", "Мюзикл"))

    title = models.CharField(max_length=100, help_text="Введите название фильма")
    release_date = models.DateField(help_text="Введите дату начала показа фильма")
    description = models.TextField(help_text="Введите оптсание фильма")
    celebrities = models.TextField(help_text="Введите основной актерский состав")
    image = models.ImageField(upload_to="static/img")
    genre = models.CharField(max_length=25, choices=GENRE_CHOICES, help_text="Выберите жанр")
    duration = models.IntegerField(default=0, help_text="Продолжительность фильма")

    age_from = models.IntegerField(help_text="Введите возрастной рейтинг", default=0)

    imdb_rating = models.FloatField(null=True, help_text="Введите рейтинг IMDB для фильма")
    rt_rating = models.FloatField(null=True, help_text="Введите рейтинг Rotten Tomatoes для фильма")

    youtube_url = models.URLField(null=True, help_text="Введите сссылку на трейлер")

    def __str__(self):
        return self.title

    class Meta:
        ordering = ("-release_date",)


class MovieFormat(models.Model):
    FORMATS = (("2d", "2D"), ("3d","3D"), ("imax2d", "IMAX 2D"), ("imax3d", "IMAX 3D"),
               ("reald2d", "RealD 2D"), ("reald3d", "RealD 3D"))
    format = models.CharField(max_length=25, choices=FORMATS, help_text="Выберите формат")


class Room(models.Model):
    room_number = models.IntegerField()
    total_rows = models.IntegerField()
    available_formats = models.ManyToManyField(MovieFormat)


class Session(models.Model):  # Сеанс на фильм
    time = models.DateTimeField(help_text="Введите дату и время сеанса")
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    format = models.ForeignKey(MovieFormat, on_delete=models.CASCADE)


class Seat(models.Model):  # Место в зале
    row_number = models.IntegerField()
    seat_number = models.IntegerField()
    room = models.ForeignKey(Room, on_delete=models.CASCADE)


class SeatForSession(models.Model):  # Место на сеансе
    seat = models.ForeignKey(Seat, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    occupied = models.BooleanField(default=False)


class Ticket(models.Model):  # Билет
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    ticket_id = models.CharField(max_length=25)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    seat = models.ManyToManyField(Seat)
    is_online = models.BooleanField()  # Куплен онлайн?
