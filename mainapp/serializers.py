from rest_framework import serializers
from mainapp.models import Movie, Session, Ticket, User


class MovieSerializer(serializers.Serializer):
    class Meta:
        model = Movie
        fields = ('title', 'release_date', 'description', 'celebrities',
                  'image', 'genre', 'imdb_rating', 'rt_rating', 'youtube_url')


class SessionSerializer(serializers.Serializer):
    model = Session
    fields = ('time', 'movie', 'room', 'format')
    depth = 1


class TicketSerializer(serializers.Serializer):
    model = Ticket


class UserSerializer(serializers.Serializer):
    model = User
    fields = ('email', 'full_name')

