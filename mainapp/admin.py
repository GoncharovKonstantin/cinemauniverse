from django.contrib import admin
from mainapp.models import *

admin.site.register(Movie)
admin.site.register(MovieFormat)
admin.site.register(Session)
admin.site.register(Seat)
admin.site.register(SeatForSession)
admin.site.register(User)
admin.site.register(Ticket)
admin.site.register(BonusCard)
admin.site.register(Room)
